from requests import post, patch, delete
from croniter import croniter
from datetime import datetime
from os import environ


def get_headers():
    notion_api_key = environ["NOTION_API_KEY"]
    return {
        "Authorization": f"Bearer {notion_api_key}",
        "Notion-Version": "2021-08-16",
    }


def destroy_object(object_id):
    headers = get_headers()
    resp = delete(
        f"https://api.notion.com/v1/blocks/{object_id}",
        headers=headers,
    )
    if resp.status_code != 200:
        raise ValueError(resp.text)


def update_object(object_id, properties):
    headers = get_headers()
    resp = patch(
        f"https://api.notion.com/v1/pages/{object_id}",
        headers=headers,
        json=dict(properties=properties),
    )
    if resp.status_code != 200:
        raise ValueError(resp.text)


def get_database_objects(database_id):
    headers = get_headers()
    resp = post(
        f"https://api.notion.com/v1/databases/{database_id}/query",
        headers=headers,
    )
    if resp.status_code != 200:
        raise ValueError(resp.text)
    return resp.json()["results"]


def tick(event, context):

    database_id = event["database_id"]
    cron_field = event.get("cron_field", "Cron Expression")
    last_run_field = event.get("last_run_field", "Last Run Timestamp")
    status_field = event.get("status_field", "Status")
    status_not_started_value = event.get("status_not_started_value", "Regular Tasks")
    status_completed_value = event.get("status_completed_value", "Completed Tasks")

    for obj in get_database_objects(database_id):
        props = obj["properties"]
        cron_expr_obj = props[cron_field]

        # If there is no cron expression set, delete the object if
        # the status is set to completed.
        if len(cron_expr_obj["rich_text"]) == 0:
            status_obj = props[status_field]
            if status_obj["select"]["name"] == status_completed_value:
                destroy_object(obj["id"])
            continue

        last_run_obj = props[last_run_field]

        # If the task is yet to be processed, initialize it by setting
        # the last run time as now.
        if len(last_run_obj["rich_text"]) == 0:
            update_object(
                obj["id"],
                {
                    last_run_field: dict(
                        rich_text=[
                            dict(text=dict(content=datetime.utcnow().isoformat()))
                        ]
                    ),
                },
            )
            continue

        last_run = datetime.fromisoformat(last_run_obj["rich_text"][0]["plain_text"])

        # Extract the cron expression and using the last run time as "now",
        # calculate when the next run-time would be.
        cron_expr = cron_expr_obj["rich_text"][0]["plain_text"]
        cron = croniter(cron_expr, last_run)
        next_datetime = cron.get_next(datetime)

        # If the next run-time is in the past, then we should reset the block
        # back to its initial state. Make sure to update the last run time as well!
        if next_datetime < datetime.utcnow():
            update_object(
                obj["id"],
                {
                    last_run_field: dict(
                        rich_text=[
                            dict(text=dict(content=datetime.utcnow().isoformat()))
                        ]
                    ),
                    status_field: dict(select=dict(name=status_not_started_value)),
                },
            )
